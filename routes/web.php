<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

	return view('front.index');

});

//Auth::routes();

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');


//seller middleware routes


Route::get('/seller/login','Auth\Seller\SellerLoginController@showLoginForm')->name('seller.login');
Route::post('/seller/login','Auth\Seller\SellerLoginController@login');
Route::post('/seller/logout','Auth\Seller\SellerLoginController@logout')->name('seller.logout');
Route::post('/seller/password/email','Auth\Seller\SellerForgotPasswordController@sendResetLinkEmail')->name('seller.password.email');
Route::get('/seller/password/reset', 'Auth\Seller\SellerForgotPasswordController@showLinkRequestForm')->name('seller.password.request');
Route::post('/seller/password/reset','Auth\Seller\SellerResetPasswordController@reset')->name('seller.password.update');
Route::get('/seller/password/reset/{token}', 'Auth\Seller\SellerResetPasswordController@showResetForm')->name('seller.password.reset');

Route::get('/seller/register','Auth\Seller\SellerRegisterController@showRegistrationForm')->name('seller.register');

Route::post('/seller/register','Auth\Seller\SellerRegisterController@register');

Route::get('/seller/email/verify','Auth\Seller\SellerVerificationController@show')->name('seller.verification.notice');
Route::get('seller/email/verify/{id}','Auth\Seller\SellerVerificationController@verify')->name('seller.verification.verify');
Route::get('/seller/email/resend','Auth\Seller\SellerVerificationController@resend')->name('seller.verification.resend');

Route::get('/seller/dashboard', 'SellerHomeController@index')->name('seller.dashboard')->middleware('seller.verified:seller,seller.verification.notice');

Route::get('/seller/business/registration', 'SellerHomeController@completeRegistration')->name('seller.business.registration')->middleware('seller.verified:seller,seller.verification.notice');

Route::post('/uniqueemail','Auth\Seller\SellerLoginController@unique_email_address')->name('unique');

//admin middleware routes


Route::get('/admin/login','Auth\Admin\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/admin/login','Auth\Admin\AdminLoginController@login');
Route::post('/admin/logout','Auth\Admin\AdminLoginController@logout')->name('admin.logout');
Route::post('/admin/password/email','Auth\Admin\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::get('/admin/password/reset', 'Auth\Admin\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('/admin/password/reset','Auth\Admin\AdminResetPasswordController@reset')->name('admin.password.update');
Route::get('/admin/password/reset/{token}', 'Auth\Admin\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
Route::get('/admin/register','Auth\Admin\AdminRegisterController@showRegistrationForm')->name('admin.register');
Route::post('/admin/register','Auth\Admin\AdminRegisterController@register');
Route::get('/admin/email/verify','Auth\Admin\AdminVerificationController@show')->name('admin.verification.notice');
Route::get('admin/email/verify/{id}','Auth\Admin\AdminVerificationController@verify')->name('admin.verification.verify');
Route::get('/admin/email/resend','Auth\admin\AdminVerificationController@resend')->name('admin.verification.resend');

Route::get('/admin/dashboard', 'AdminHomeController@index')->name('admin.dashboard')->middleware('admin.verified:admin,admin.verification.notice');



Route::group(['middleware' => ['auth:admin']], function () { 

	/*admin service Routes*/
	
	Route::get('/admin/services/all' , 'admin\services\ServiceController@index')->name('allServices');
	Route::post('/admin/service/create' , 'admin\services\ServiceController@create')->name('addServiceType');
	Route::get('/admin/service/type/add' , 'Admin\services\ServiceController@add')->name('addService');
	Route::get('/admin/service/edit/{id}' , 'Admin\services\ServiceController@edit')->name('editService');
	Route::post('/admin/service/update' , 'Admin\services\ServiceController@update')->name('updateService');
	Route::post('/admin/service/delete' , 'Admin\services\ServiceController@destroy')->name('deleteService'); 

});












Route::get('/about', function () {

	return view('front.about');

});

Route::get('/categories', function () {

	return view('front.categories');

});

Route::get('/category/detail', function () {

	return view('front.catagories-detail');

});

Route::get('/seller/join', function () {

	return view('front.pages.becomeaseller.sellerstartup');

});

