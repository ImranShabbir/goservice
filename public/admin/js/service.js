console.log('file is loaded');

let make_id = null;

$(document).ready(function(){ 
   
    $("#addServiceBtn").click(function(){
       
       let service_name = $("#service_name").val();

       console.log('name',name);
       
            $.ajax({
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: BASE_URL+'/admin/service/create',
                type: 'POST',
                data: {
                    'service_name' : service_name                  
                   
                },
               success:function(data){

                console.log(data);
                                     
                   if(data.success) {
                       new PNotify({
                           title: 'New service added successfully.',
                           type: 'success',
                           hide: true,
                           delay: 4000,
                           animation: 'fade',
                           animateSpeed: 'slow'
                       });

                        document.getElementById("addService").reset();
                   //     setTimeout((function() {
                   //      window.location.href=BASE_URL+"/admin/make";
                   //    }), 1000);
                    }else{
                        printErrorMsg(data.error);

                    }
               
             }
           });
       
    });

    //Product type update function
    $("#updateBtnService").click(function(){
       
       let service_id = $("#service_id").val();
       let service_name = $("#service_name").val();

       console.log('name',service_name);
       console.log('id',service_id);

            $.ajax({
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: BASE_URL+'/admin/service/update',
                type: 'POST',
                data: {
                    'service_id' : service_id,
                    'service_name' : service_name                  
                   
                },
               success:function(data){

                console.log(data);
                                     
                   if(data.success) {
                       new PNotify({
                           title: 'Service updated successfully.',
                           type: 'success',
                           hide: true,
                           delay: 4000,
                           animation: 'fade',
                           animateSpeed: 'slow'
                       });

                    setTimeout((function() {
                        window.location.href=BASE_URL+"/admin/services/all";
                    }), 1000);
                    }else{
                        printErrorMsg(data.error);

                    }
               
             }
           });
       
    });


   
    //Error function
    function printErrorMsg (msg) {
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display','block');
        $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
        });
    }
 
    
});

//Product Make Delete function
function deleteMake(make_id)
{
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this Make",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: BASE_URL+'/admin/deleteproductmake',
                    type: 'POST',
                    data: {
                        'id' : make_id
                    },
                    success:function(data){
                        if(data.success) {
                            new PNotify({
                                title: 'Make',
                                text: data.success,
                                type: 'success'
                            });
                            setTimeout(function(){
                                window.location.reload();
                            }, 1000);
                        }
                    }
                });
            } else {
                // new PNotify({
                //     text: 'Your Administrator account is Safe.',
                //     type: 'success',
                //     hide: true,
                //     delay: 4000,
                //     animation: 'fade',
                //     animateSpeed: 'slow'
                // });
            }
        });
}

//Product Type Delete function
function deleteService(service_id)
{

  console.log('id',service_id);
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this Service",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: BASE_URL+'/admin/service/delete',
                    type: 'POST',
                    data: {
                        'id' : service_id
                    },
                    success:function(data){
                        if(data.success) {
                            new PNotify({
                                title: 'Type',
                                text: data.success,
                                type: 'success'
                            });
                            setTimeout(function(){
                                window.location.reload();
                            }, 1000);
                        }
                    }
                });
            } else {
                function printErrorMsg (msg) {
                    $(".print-error-msg").find("ul").html('');
                    $(".print-error-msg").css('display','block');
                    $.each( msg, function( key, value ) {
                        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
                    });
                }
            }
        });
}
