$(document).ready(function(e) {
$('.navbar-toggler').on('click', function(e) {
$('body').toggleClass('nav-open');
e.stopPropagation();
});
});

$(".navbar-collapse").click(function(e){
  e.stopPropagation();
});

$(document).click(function(e) {
$('body').removeClass('nav-open');
});

// toggle menu button

function myFunction(x) {
  x.classList.toggle("change");
}

// Smooth Scrolling
$('a#scroll-down').click(function(){
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top
    }, 1500);
    return false;
});

// Best Seller 
$('.bestseller-items').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 991.98,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 767.98,
      settings: {
        slidesToShow: 1
      }
    }
  ]
});

// Services 
$('.services-holder').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    mobileFirst: true,
    responsive: [
    {
      breakpoint: 767.98,
      settings: "unslick"
    }
  ]
});

// product Detail slider

$('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slider-nav',
   autoplay: false
});
$('.slider-nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: false,
  centerMode: false,
  focusOnSelect: true

});

// Dropdown menu
$(document).ready(function(e) {
  $('#dropdown a.opener').on('click', function(e) {
  $('#dropdown').toggleClass('active');
  e.stopPropagation();
  });
});
$(".dropdown-menu").click(function(e){
  e.stopPropagation();
});

$(document).click(function(e) {
$('#dropdown').removeClass('active');
});

$(document).ready(function(e) {
$('.flow-wrapper .form-group input').on('click', function(e) {
$(this).parent().parent().toggleClass('open-options');
e.stopPropagation();
});
});
