@extends('layouts.sellerlanding')

@section('content')
<section class="login-block overflow-hidden" style="background-image: url({{ URL::asset('images/login-bg.png')}})">
    <div class="container">
        <div class="row">
            <div class="col-12">
              <div class="heading text-center mb-lg-5">
                <h1 class="login-title text-center" id="staticBackdropLabel">Welcome To  <span class="orange">Seller Account</span></h1>
                <p class="grey">Log In To Continue</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="login-content">
        
                
                    <form method="POST" action="{{ route('seller.login') }}">
                        @csrf

                        <div class="input-group mb-4">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="{{ __('Email Address') }}">

                            <div class="input-group-prepend">
                                <div class="input-group-text border-0"><span class="icon-mail"></span></div>
                            </div>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="input-group mb-3">


                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="{{ __('Password') }}">
                            <div class="input-group-prepend">
                                <div class="input-group-text border-0"><span class="icon-padlock"></span></div>
                            </div>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror

                        </div>

                        <div class="form-group mb-lg-5 mb-3">
                            <label class="smart-label" for="remember">
                                {{ __('Remember Me') }}

                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <span class="checkmark"></span>  
                            </label>      



                        </div>

                        <div class="form-group border-bottom pb-4">

                            <button type="submit" class="btn w-100 mb-3">
                                {{ __('Login') }} <span></span><span></span><span></span><span></span>
                            </button>

                            @if (Route::has('password.request'))
                            <a class="forgot-password text-center d-block dark-grey" href="{{ route('seller.password.request') }}">
                                {{ __('Forgot Password?') }}
                            </a>
                            @endif
                        </div>
                        <div class="form-group text-center ">        
                            <p>Don’t have an account<span class="question">?</span> 
                                <a href="{{url('/seller/register')}}" class="blue signp-btn" id="goservice-signup">Sign up </a>
                            </p>


                        </div>
                    </form>

            </div>        
        </div>        
            
       
    </div>
</div>
</section>
@endsection
