@extends('layouts.sellerlanding')
@section('content')


<section class="login-block signp-block overflow-hidden" style="background-image: url(../images/login-bg.png);">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="heading text-center mb-lg-5">
          <h1 class="login-title text-center" id="staticBackdropLabel">Create Your<span class="orange"> Merchant
              Account</span></h1>
          <p class="grey">Tell us about your business so we start your campaign.</p>
        </div>
      </div>
    </div>
    <div class="row">
      
      <div class="col-md-7">
        <form method="POST" action="{{ route('seller.register') }}">
          @csrf
          <!-- Step-0 -->
          <div id="step-zero" title="first" class="s0 slide active">
            <div class="input-group mb-4">
              <input type="text" class="form-control mr-md-2 mb-2 mb-md-0 @error('firstname') is-invalid @enderror""
                id="firstname" placeholder="{{ __('First Name') }}" name="firstname" value="{{ old('firstname') }}"
                required autocomplete="firstname" autofocus>
              @error('firstname')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
              <div class="input-group-prepend">
                <div class="input-group-text border-0"><span class="icon-admin"></span></div>
              </div>
            </div>
            {{-- <div class="alert alert-danger w-100 mb-4" role="alert" id="firstnameerror">
              First name is required!
            </div> --}}
            <div class="input-group mb-4">
              <input type="text" class="form-control mr-md-2 mb-2 mb-md-0 @error('lastname') is-invalid @enderror"
                id="lastname" placeholder="{{ __('Last Name') }}" name="lastname" value="{{old('lastname')}}" required
                autocomplete="lastname" autofocus>
              @error('lastname')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
              <div class="input-group-prepend">
                <div class="input-group-text border-0"><span class="icon-admin"></span></div>
              </div>
            </div>
            {{-- <div class="alert alert-danger w-100 mb-4" role="alert" id="lastnameerror">
              Last name is required!
            </div> --}}
            <div class="input-group mb-4">
              <input type="email" class="form-control mr-md-2 mb-2 mb-md-0 @error('email') is-invalid @enderror"
                id="email" placeholder="{{ __('E-Mail Address') }}" name="email" value="{{ old('email') }}" required
                autocomplete="email">
              @error('email')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
              <div class="input-group-prepend">
                <div class="input-group-text border-0"><span class="icon-mail"></span></div>
              </div>
            </div>
            {{-- <div class="alert alert-danger w-100 mb-4" role="alert" id="emailerror">
              Email is required!
            </div> --}}
            <div class="input-group mb-3">
              <input type="password" class="form-control mr-md-2 mb-2 mb-md-0 @error('password') is-invalid @enderror"
                id="password" placeholder="{{ __('Create Password') }}" name="password" required
                autocomplete="new-password">
              @error('password')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
              <div class="input-group-prepend">
                <div class="input-group-text border-0"><span class="icon-padlock"></span></div>
              </div>
            </div>
            {{-- <div class="alert alert-danger w-100 mb-4" role="alert" id="passworderror">
              Password is required!
            </div> --}}
            <div class="input-group mb-3">
              <input type="Password" id="confirm" class="form-control mr-md-2 mb-2 mb-md-0"
                placeholder="{{ __('Confirm Password') }}" name="password_confirmation" required
                autocomplete="new-password">

              <div class="input-group-prepend">
                <div class="input-group-text border-0"><span class="icon-padlock"></span></div>
              </div>
            </div>
            {{-- <div class="alert alert-danger w-100 mb-4" role="alert" id="confirmpassworderror">
              Confirm Password is required!
            </div> --}}
            <div class="input-group mb-3">
              <input type="text" id="business_name"
                class="form-control mr-md-2 mb-2 mb-md-0 @error('business_name') is-invalid @enderror"
                placeholder="{{ __('Business Name') }}" name="business_name" value="{{ old('business_name') }}" required
                autocomplete="business_name">
              @error('business_name')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
              <div class="input-group-prepend">
                <div class="input-group-text border-0"><span class="icon-admin"></span></div>
              </div>
            </div>
            {{-- <div class="alert alert-danger w-100 mb-4" role="alert" id="businesserror">
              Business Name is required!
            </div> --}}
            <div class="input-group mb-3">
              <input type="text" id="business_description"
                class="form-control mr-md-2 mb-2 mb-md-0 @error('business_description') is-invalid @enderror"
                placeholder="{{ __('Business Description') }}" name="business_description"
                value="{{ old('business_description') }}" required autocomplete="business_description">
              @error('business_description')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
              <div class="input-group-prepend">
                <div class="input-group-text border-0"><span class="icon-admin"></span></div>
              </div>
            </div>
            {{-- <div class="alert alert-danger w-100 mb-4" role="alert" id="descriptionerror">
              Business description is required!
            </div> --}}
            <div class="input-group mb-3">
              <input type="text" id="business_location"
                class="form-control mr-md-2 mb-2 mb-md-0 @error('business_location') is-invalid @enderror"
                placeholder="{{ __('Business Location') }}" name="business_location"
                value="{{ old('business_location') }}" required autocomplete="business_location">
              @error('business_location')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
              <div class="input-group-prepend">
                <div class="input-group-text border-0"><span class="icon-admin"></span></div>
              </div>
            </div>
            {{-- <div class="alert alert-danger w-100 mb-4" role="alert" id="locationerror">
              Business Location is required!
            </div> --}}
            <div class="input-group mb-3">
              <input type="text" id="business_website"
                class="form-control mr-md-2 mb-2 mb-md-0 @error('business_website') is-invalid @enderror"
                placeholder="{{ __('Business Website') }}" name="business_website" value="{{ old('business_website') }}"
                required autocomplete="business_website">
              @error('business_website')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
              <div class="input-group-prepend">
                <div class="input-group-text border-0"><span class="icon-admin"></span></div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="text" id="phone"
                class="form-control mr-md-2 mb-2 mb-md-0 @error('phone') is-invalid @enderror"
                placeholder="{{ __('Business Phone') }}" name="phone" value="{{ old('phone') }}" required
                autocomplete="phone">
              @error('phone')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
              <div class="input-group-prepend">
                <div class="input-group-text border-0"><span class="icon-phone"></span></div>
              </div>
            </div>
            <input type="hidden" name="user_type" value="seller">
            <div class="form-group border-bottom pb-4">
              <button type="submit" class="btn w-100 mb-3">{{ __('Next') }}
            <span></span><span></span><span></span><span></span></button>
            <!-- <a href="#" class="forgot-password text-center d-block dark-grey">Forgot password?</a> -->
          </div>
          <div class="form-group text-center ">
            <p>Already have an account<span class="question">?</span> <a href="{{url('/seller/login')}}"
                class="blue signp-btn" id="goservice-signup">Log In</a></p>
          </div>

      </div>



      

      </form>

      
    </div>
    
  </div>
  </div>

</section>
@endsection