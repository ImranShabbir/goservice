@extends('layouts.app')

@section('content')


<main>
   <!-- Login Form -->
   <section class="login-block signp-block overflow-hidden" style="background-image: url(images/login-bg.png);">
     <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="heading text-center mb-lg-5">
            <h1 class="login-title text-center" id="staticBackdropLabel">Welcome To <span class="orange"> Go Service</span></h1>
            <p class="grey">Sign Up To Continue</p>
          </div>
        </div>
      </div>
       <div class="row">
         <div class="col-md-6 offset-md-3">
           <div class="signp-content">
              <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="input-group mb-4">
                  <input type="text" class="form-control mr-md-2 mb-2 mb-md-0 @error('firstname') is-invalid @enderror" id="firstname" placeholder="{{ __('First Name') }}" name="firstname" value="{{ old('firstname') }}" required autocomplete="firstname" autofocus>
                    @error('firstname')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  <div class="input-group-prepend">
                    <div class="input-group-text border-0"><span class="icon-admin"></span></div>
                  </div>
                </div>
                <div class="input-group mb-4">
                  <input type="text" class="form-control mr-md-2 mb-2 mb-md-0 @error('lastname') is-invalid @enderror" id="lastname" placeholder="{{ __('Last Name') }}" name="lastname" value="{{old('lastname')}}" required autocomplete="lastname" autofocus>
                    @error('lastname')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  <div class="input-group-prepend">
                    <div class="input-group-text border-0"><span class="icon-admin"></span></div>
                  </div>
                </div>
                <div class="input-group mb-4">
                  <input type="text" class="form-control mr-md-2 mb-2 mb-md-0 @error('phone') is-invalid @enderror" id="phone" placeholder="{{ __('Phone') }}" name="phone" value="{{ old('phone') }}" required autocomplete="phone">
                    @error('phone')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  <div class="input-group-prepend">
                    <div class="input-group-text border-0"><span class="icon-phone"></span></div>
                  </div>
                </div>
                <div class="input-group mb-4">
                  <input type="email" class="form-control mr-md-2 mb-2 mb-md-0 @error('email') is-invalid @enderror" id="email" placeholder="{{ __('E-Mail Address') }}" name="email" value="{{ old('email') }}" required autocomplete="email">
                  @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  <div class="input-group-prepend">
                    <div class="input-group-text border-0"><span class="icon-mail"></span></div>
                  </div>
                </div>
                <div class="input-group mb-3">
                  <input type="password" class="form-control mr-md-2 mb-2 mb-md-0 @error('password') is-invalid @enderror" id="password" placeholder="{{ __('Create Password') }}" name="password" required autocomplete="new-password">
                  @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  <div class="input-group-prepend">
                    <div class="input-group-text border-0"><span class="icon-padlock"></span></div>
                  </div>
                </div>
                <div class="input-group mb-3">
                  <input type="Password" id="password-confirm" class="form-control mr-md-2 mb-2 mb-md-0" placeholder="{{ __('Confirm Password') }}" name="password_confirmation" required autocomplete="new-password">
                  
                  <div class="input-group-prepend">
                    <div class="input-group-text border-0"><span class="icon-padlock"></span></div>
                  </div>
                </div>
                <input type="hidden" name="user_type" value="buyer">
                <div class="form-group mb-lg-5 mb-3">
                  <label class="smart-label">I don't want to receive marketing messages from Go Service. I can also opt out of receiving these at any time in my account settings or via the link in the message.
                    <input type="checkbox">
                    <span class="checkmark"></span>
                  </label>
                </div>
                <div class="form-group border-bottom pb-4">
                  <button type="submit" class="btn w-100 mb-3">{{ __('Register') }} <span></span><span></span><span></span><span></span></button>
                  <!-- <a href="#" class="forgot-password text-center d-block dark-grey">Forgot password?</a> -->
                </div>
                <div class="form-group text-center ">
                 <p>Already have an account<span class="question">?</span> <a href="{{url('/login')}}" class="blue signp-btn">Log In</a></p> 
                </div>
              </form>
            </div>
         </div>
       </div>
     </div>
   </section>
  </main>




<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="firstname" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>

                            <div class="col-md-6">
                                <input id="firstname" type="text" class="form-control @error('firstname') is-invalid @enderror" name="firstname" value="{{ old('firstname') }}" required autocomplete="firstname" autofocus>

                                @error('firstname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

                            <div class="col-md-6">
                                <input id="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" required autocomplete="lastname" autofocus>

                                @error('lastname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone">

                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <input type="hidden" name="user_type" value="buyer">

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection
