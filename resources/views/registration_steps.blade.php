@extends('layouts.sellerauth')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <!-- Step-1 -->
            <div class="col-md-12" id="multiSteps">
      <div id="step-zero" title="first" class="s0 slide flow-wrapper overflow-hidden text-center active">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="step">
                <div class="head mb-5">
                  <strong class="text-uppercase d-block">What INDUSTRY do you provide?</strong>
                  <span class="d-block">You can edit these later if you'd like.</span>
                </div>

                <div class="form-row flex-column mb-5">
                  <div class="form-group">
                    <label class="smart-label">Alarms
                      <input type="checkbox" name="services">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label class="smart-label">Carpets
                      <input type="checkbox" name="services">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label class="smart-label">HVAC
                      <input type="checkbox" name="services">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label class="smart-label">Painting
                      <input type="checkbox" name="services">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label class="smart-label">Pest Control
                      <input type="checkbox" name="services">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label class="smart-label">Plumbing
                      <input type="checkbox" name="services">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label class="smart-label">Solar
                      <input type="checkbox" name="services">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label class="smart-label">Windows
                      <input type="checkbox" name="services">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                </div>
                {{-- <button type="submit" class="btn bg-blue">next</button> --}}

              </div>
            </div>
          </div>
        </div>
      </div>


      <!-- Step-2 -->
      <div id="step-one" title="second" class="s1 slide flow-wrapper overflow-hidden text-center">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="step">
                <div class="head mb-5">
                  <strong class="text-uppercase d-block">What SERVICES do you provide?</strong>
                  <span class="d-block">You can edit these later if you'd like.</span>
                </div>

                <div class="form-row flex-column mb-5">
                  <div class="form-group">
                    <label class="smart-label">Drain
                      <input type="checkbox">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                    <div class="second-level-options levels mt-3">
                      <div class="form-group">
                        <label class="smart-label">Drain
                          <input type="checkbox">
                          <span class="checkmark upsize-checkmark"></span>
                        </label>
                        <div class="third-level-options levels mt-3">
                          <div class="form-group">
                            <label class="smart-label">Drain
                              <input type="checkbox">
                              <span class="checkmark upsize-checkmark"></span>
                            </label>
                          </div>
                          <div class="form-group">
                            <label class="smart-label">Drain
                              <input type="checkbox">
                              <span class="checkmark upsize-checkmark"></span>
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="smart-label">Drain
                          <input type="checkbox">
                          <span class="checkmark upsize-checkmark"></span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="smart-label">Faucets, Fixtures, Pipes
                      <input type="checkbox">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label class="smart-label">Shower
                      <input type="checkbox">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label class="smart-label">Toilet
                      <input type="checkbox">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label class="smart-label">Gas Piping
                      <input type="checkbox">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label class="smart-label">Sink
                      <input type="checkbox">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label class="smart-label">Pumps
                      <input type="checkbox">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label class="smart-label">Water Softening &amp; Purification
                      <input type="checkbox">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label class="smart-label">Gass Piping
                      <input type="checkbox">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label class="smart-label">Septic Systems, Sewers, Water Mains
                      <input type="checkbox">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label class="smart-label">Sprinkle Systems
                      <input type="checkbox">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label class="smart-label">Water Heater
                      <input type="checkbox">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label class="smart-label">Boilers and Radiatior Heating Systems
                      <input type="checkbox">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label class="smart-label">Hot Tub or Spa
                      <input type="checkbox">
                      <span class="checkmark upsize-checkmark"></span>
                    </label>
                  </div>
                </div>
                {{-- <div class="form-group p-0 border-0">
                  <button type="submit" class="btn mr-2">back</button>
                  <button type="submit" class="btn bg-blue">next</button>
                </div> --}}

              </div>
            </div>
          </div>
        </div>
      </div>


      <div id="step-two" class="s2 slide flow-wrapper overflow-hidden text-center">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="step">
                <div class="head mb-5">
                  <strong class="text-uppercase d-block">Where do you work with your customers?</strong>
                  <span class="d-block">You can edit these later if you'd like.</span>
                </div>

                <div class="form-row row-custom">
                  <div class="col-md-6">
                    <label for="inputGroupSelect01">Add Business Address</label>
                    <select id="inputGroupSelect01" class="custom-select">
                      <option selected="">6995 S Shore Rd, Polson, MT, 59860</option>
                      <option value="1">6995 S Shore Rd, Polson, MT, 59860</option>
                      <option value="2">6995 S Shore Rd, Polson, MT, 59860</option>
                      <option value="3">6995 S Shore Rd, Polson, MT, 59860</option>
                    </select>
                  </div>
                  <div class="col-md-6">
                    <label for="inputGroupSelect01">How far are you willing to travel?</label>
                    <select class="custom-select" id="inputGroupSelect02">
                      <option selected="">25 mIles</option>
                      <option value="1">25 mIles</option>
                      <option value="2">25 mIles</option>
                      <option value="3">25 mIles</option>
                    </select>
                  </div>
                </div>
                <div class="form-row flex-column align-items-center">
                  <div class="map-hol form-group w-100 p-0 mb-5">
                    <div class="embed-responsive embed-responsive-16by9">
                      <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26358303.642785776!2d-113.69837289858118!3d36.25002656401592!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited%20States!5e0!3m2!1sen!2s!4v1581595817483!5m2!1sen!2s"
                        allowfullscreen></iframe>
                    </div>
                  </div>
                  <div class="form-group p-0 border-0">
                    {{-- <button type="submit" class="btn mr-2">cancel</button> --}}
                    <button type="submit" class="btn bg-blue">save</button>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="text-center">
      
      <button type="button" class="btn mr-2" id="previous">previous</button> 
      <button type="button" class="btn bg-blue" id="next">next</button> 
      </div>
      </div>      
  </div>
    </div>
</div>
@endsection