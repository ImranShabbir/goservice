<!doctype html>
<html lang="en">

<head>
  <!-- Meta Tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">
  <link href="{{URL::asset('images/favicon.png')}}" rel="icon" />
  <!-- Slick Slider CSS -->
  <link rel="stylesheet" href="{{ URL::asset('css/slick.css') }}">
  <!-- Boostrap -->
  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
  <!-- Main CSS -->
  <link rel="stylesheet" href="{{ URL::asset('css/main.css') }}">
  <!-- <link rel="stylesheet" id="css-main" href="{{ URL::asset('css/dashmix.min.css') }}"> -->


  @yield('css')

</head>

<body class="body">

  <header id="header">
    <div class="top-bar">
      <div class="container">
        <ul class="d-flex justify-content-center justify-content-lg-end align-iems-center">



          <li><a href="{{url('/seller/login')}}"><span class="icon-user"></span>Sign In</a></li>
          <li><a href="{{url('/seller/register')}}"><span class="icon-user"></span>Get Started</a></li>

        </ul>
      </div>
    </div>
    <div class="main-nav-block" style="min-height:50px;">
      <div class="container">
        <div class="row ">
          <div class="col-12">
            <div class="header-block d-flex justify-content-between align-items-center">
              <div class="logo">
                <a class="navbar-brand d-block" href="{{url('/')}}">
                  <figure>
                    <img src="{{asset('images/logo.png')}}" alt="site logo">
                  </figure>
                </a>
              </div>
              <nav class="navbar navbar-expand-lg">
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbar"
                  aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="bar top"></span>
                  <span class="bar middle"></span>
                  <span class="bar bottom"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbar">
                  <ul class="navbar-nav w-100 d-lg-flex justify-content-end">

                  </ul>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <div id="wrapper">
    @yield('content')
  </div>

  <!-- Page Footer -->
  <!-- Optional JavaScript -->

  <script src="{{ URL::asset('js/jquery-3.4.1.min.js') }}"></script>
  <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
  <script src="{{URL::asset('js/dashmix.core.min.js')}}"></script>
  <script src="{{ URL::asset('js/slick.min.js') }}"></script>
  <script src="{{ URL::asset('js/custom.js') }}"></script>
  
  
  
  <!-- <script src="{{ URL::asset('js/slick.min.js') }}"></script>
  -->
  <!-- <script src="{{ URL::asset('js/popper.min.js') }}"></script> -->

  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>

  <!-- <script src="{{URL::asset('js/dashmix.core.min.js')}}"></script> -->

  <script src="{{URL::asset('js/dashmix.app.min.js')}}"></script>

  <!-- Page JS Plugins -->
  <!-- <script src="{{URL::asset('js/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
  <script src="{{URL::asset('js/plugins/chart.js/Chart.bundle.min.js')}}"></script> -->

  <!-- Page JS Code -->
  <!-- <script src="{{URL::asset('js/pages/be_pages_dashboard.min.js')}}"></script> -->

  <!-- Page JS Helpers (jQuery Sparkline plugin) -->
  <!-- <script>jQuery(function(){ Dashmix.helpers('sparkline'); });</script> -->

</body>

</html>