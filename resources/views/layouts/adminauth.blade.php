<!doctype html>
<html lang="en">
<head>
	<!-- Meta Tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">



	<!-- Google Font -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">  -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet"> 
	<link href="{{URL::asset('images/favicon.png')}}" rel="icon" />
	<!-- Slick Slider CSS -->
	<link rel="stylesheet" href="{{ URL::asset('css/slick.css') }}">

	<!-- Boostrap -->

	<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">


	<!-- Main CSS -->
	<link rel="stylesheet" href="{{ URL::asset('css/main.css') }}"> 


	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

	@yield('css')

</head>
<body class="body">
	<!-- <header id="header">
    
    <div class="main-nav-block">
      <div class="container">
        <div class="row">
          <div class="col-12">
            
          </div>
        </div>
      </div>
    </div>
  </header> -->
	<div id="wrapper">
		@yield('content')
	</div>


	<!-- Optional JavaScript -->
	<script src="{{ URL::asset('js/jquery-3.4.1.min.js') }}"></script>
	<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ URL::asset('js/slick.min.js') }}"></script>
<!-- <script src="{{ URL::asset('js/slick.min.js') }}"></script>
	<script src="{{ URL::asset('js/custom.js') }}"></script> -->
	<!-- <script src="{{ URL::asset('js/popper.min.js') }}"></script> -->

</body>
</html>