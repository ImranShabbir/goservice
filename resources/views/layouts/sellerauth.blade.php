<!doctype html>
<html lang="en">

<head>
  <!-- Meta Tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Google Font -->
  <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">  -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">
  <link href="{{URL::asset('images/favicon.png')}}" rel="icon" />
  <!-- Slick Slider CSS -->
  <link rel="stylesheet" href="{{ URL::asset('css/slick.css') }}">
  <!-- Boostrap -->
  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
  <!-- Main CSS -->
  <link rel="stylesheet" href="{{ URL::asset('css/main.css') }}">

  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>

  @yield('css')

</head>

<body class="body">
  <header id="header">
    <div class="top-bar">
      <div class="container">
        <ul class="d-flex justify-content-center justify-content-lg-end align-iems-center">

          <?php
          $getuser = '';
            if(Auth::check()) {
              if(Auth::guard('seller')->user()){
                $getuser = Auth::guard('seller')->user();
                $getuser = $getuser->firstname . ' '. $getuser->lastname;
            }
          }
          ?>
          <li class="nav-item dropdown">
            <a id="navbarDropdown" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false" v-pre>
              {{$getuser}} <span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              {{-- <a class="dropdown-item" href="{{ route('home') }}">
                {{ __('Dashboard') }}
              </a> --}}
              <a class="dropdown-item" href="{{ route('seller.logout') }}" onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
              </a>

              <form id="logout-form" action="{{ route('seller.logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
            </div>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-nav-block" style="min-height:50px;">
      <div class="container">
        <div class="row ">
          <div class="col-12">
            <div class="header-block d-flex justify-content-between align-items-center">
              <div class="logo">
                <a class="navbar-brand d-block" href="{{url('/')}}">
                  <figure>
                    <img src="{{asset('images/logo.png')}}" alt="site logo">
                  </figure>
                </a>
              </div>
              <nav class="navbar navbar-expand-lg">
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbar"
                  aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="bar top"></span>
                  <span class="bar middle"></span>
                  <span class="bar bottom"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbar">
                  <ul class="navbar-nav w-100 d-lg-flex justify-content-end">

                  </ul>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <div id="wrapper">
    @yield('content')
  </div>

  <!-- Page Footer -->
  @include('front.footer')

  <!-- Optional JavaScript -->
  <script src="{{ URL::asset('js/jquery-3.4.1.min.js') }}"></script>
  <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ URL::asset('js/slick.min.js') }}"></script>
  <script src="{{ URL::asset('js/custom.js') }}"></script>
  <script src="{{ asset('js/flow.js') }}"></script>
  <!-- <script src="{{ URL::asset('js/slick.min.js') }}"></script>

<!-- <script src="{{ URL::asset('js/popper.min.js') }}"></script> -->

</body>

</html>