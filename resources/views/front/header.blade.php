<!-- Header -->

<header id="header">
    <div class="top-bar">
      <div class="container">
        <ul class="d-flex justify-content-center justify-content-lg-end align-iems-center">
          <li><a href="#"><span class="icon-heart"></span>My Wishlist</a></li>
          <li><a href="#"><span class="icon-cart"></span>Cart</a></li>
          <li><a href="#"><span class="icon-shop"></span>Shop a Service</a></li>
          <li><a href="{{url('/seller/join')}}"><span class="icon-bullhorn"></span>Become a Seller</a></li>
          <li><a href="#"><span class="icon-text"></span>Messages</a></li>
          <?php
          $getuser = '';
     		if(Auth::check()) {
            if(Auth::guard('web')->user()){
    			$getuser = Auth::guard('web')->user();
				$getuser = $getuser->firstname . ' '. $getuser->lastname;
      }
      if(Auth::guard('seller')->user()){
          $getuser = Auth::guard('seller')->user();
        $getuser = $getuser->firstname . ' '. $getuser->lastname;
      }
			?>



				<li class="nav-item dropdown">
                            <a id="navbarDropdown" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{$getuser}} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            	<a class="dropdown-item" href="{{ route('home') }}">
                                    {{ __('Dashboard') }}
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>

			<?php }else{?>

				<li><a href="{{url('/login')}}"><span class="icon-user"></span>Sign In</a></li>
			<?php }
          	?>

        </ul>
      </div>
    </div>
    <div class="main-nav-block">
      <div class="container">
        <div class="row ">
          <div class="col-12">
            <div class="header-block d-flex justify-content-between align-items-center">
              <div class="logo">
                <a class="navbar-brand d-block" href="{{url('/')}}">
                  <figure>
                    <img src="{{asset('images/logo.png')}}" alt="site logo">
                  </figure>
                </a>
              </div>
              <nav class="navbar navbar-expand-lg">
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="bar top"></span>
                    <span class="bar middle"></span>
                    <span class="bar bottom"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbar">
                  <ul class="navbar-nav w-100 d-lg-flex justify-content-end">
                      <li class="nav-item"><a class="nav-link text-capitalize" href="{{url('/categories')}}">Alarms</a> </li>
                      <li class="nav-item"><a class="nav-link text-capitalize" href="{{url('/categories')}}">Carpets</a> </li>
                      <li class="nav-item"><a class="nav-link text-capitalize" href="{{url('/categories')}}">HVAC</a></li>
                      <li class="nav-item"><a class="nav-link text-capitalize" href="{{url('/categories')}}">Painting</a></li>
                      <li class="nav-item"><a class="nav-link text-capitalize" href="{{url('/categories')}}">Pest Control</a></li>
                      <li class="nav-item"><a class="nav-link text-capitalize" href="{{url('/categories')}}">Plumbing</a></li>
                      <li class="nav-item"><a class="nav-link text-capitalize" href="{{url('/categories')}}">Solar</a></li>
                      <li class="nav-item"><a class="nav-link text-capitalize" href="{{url('/categories')}}">Windows</a></li>
                      <li class="nav-item"><a class="nav-link text-capitalize" href="#">View All Categories</a></li>
                    </ul>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>