@extends('layouts.app')

@section('content')

<main>
    <section class="visual-block inner-visual overflow-hidden text-center position-relative" style="background-image: url(images/banner.jpg);">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="content-hol">
              <h1 class="m-0">Millions of customers trust <br class="d-none d-md-block">go service to connect with <br class="d-none d-md-block">the best pros.</h1>
            </div>
          </div>
        </div>
      </div>
    </section>

     <!-- Home Care Block -->
    <section class="homecare-block about-homecare-block overflow-hidden text-center about-go">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="heading mb-lg-5">
              <h1>about go service</h1>
              <p>Go Service is an online marketplace where home service providers and homeowners connect to buy, sell, and schedule home improvement projects at a fixed price. Book your favorite pros at the best prices. No lead generation. No wait times.</p>
            </div>
          </div>
        </div>
        <div class="row align-items-lg-center about-service">
          <div class="col-lg-6 pr-lg-5">
            <div class="img-hol mb-5 mb-lg-0 position-relative">
              <img class="w-100" src="images/about1.jpg" alt="home service">
            </div>
          </div>
          <div class="col-lg-6 pl-lg-5">
            <div class="content-hol text-left">
              <h2>OUR STORY</h2>
              <p>The Go Service story started back in 2018 when the founders turned to the internet to generate online leads for a pest control company they worked for. However, after just a few months of being in the lead gen business, they learned that lead generation was not the answer to their company’s problems.</p>
              <p>The company did not need leads, it needed closed accounts. This realization began the process of brainstorming and creating a marketplace where services could be bought, sold, and scheduled rather than only settling at the lead level.</p>
            </div>
          </div>
        </div>
      </div>
    </section>


     <!-- Helping Communities -->
    <section class="industries-block overflow-hidden text-center position-relative communities-sec">
      <div class="row">
        <div class="col-12 heading">
          <h1>Helping local communities <br class="d-none d-lg-block"> thrive.</h1>
          <p>Go Service is an active member of the community, helping service providers across many industries acquire and maintain customer <br class="d-none d-md-block">
            accounts and relationships.</p>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="community-block mb-4 mb-md-0">
              <Strong class="number d-block orange">800,000</Strong>
              <span class="d-block">New customers each year</span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="community-block mb-4 mb-md-0">
              <Strong class="number d-block orange">30,000+</Strong>
              <span class="d-block">Active pros</span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="community-block mb-4 mb-md-0">
              <Strong class="number d-block orange">$90 million</Strong>
              <span class="d-block">Earned by pros</span>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Try Services -->
    <section class="services-block-hol overflow-hidden text-center position-relative">
      <div class="container">
        <div class="row">
          <div class="col-12 heading mb-4">
            <h1>Try GO SERVICES today</h1>
            <p>Become a part of our community and see how Go Service can help your business or home improvement project today.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="btn-hol d-flex justify-content-center flex-column flex-md-row">
              <a href="{{url('/')}}" class="btn">Explore Go Service</a>
              <a href="{{url('/seller/login')}}" class="btn">Join as a Pro</a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  @endsection