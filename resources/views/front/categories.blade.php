@extends('layouts.app')

@section('content')

<main>
    <section class="visual-block inner-visual overflow-hidden text-center position-relative" style="background-image: url(images/banner.jpg);">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="content-hol">
              <h1 class="m-0">Alarm Systems</h1>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Most Popular Services -->
    <section class="services-block categories overflow-hidden text-center">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="heading mb-5">
              <h1>Best Alarm Systems Services <br class="d-none d-md-block"> near you</h1>
              <div class="category-search">
                <form action="#">
                  <div class="form-row">
                    <div class="col-md-3 mb-2">
                      <select class="custom-select">
                        <option selected>service type</option>
                        <option value="1">type</option>
                        <option value="2">type</option>
                        <option value="3">type</option>
                      </select>
                    </div>
                    <div class="col-md-3 mb-2">
                      <select class="custom-select">
                        <option selected>brands</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                      </select>
                    </div>
                    <div class="col-md-3 mb-2">
                      <select class="custom-select">
                        <option selected>prices</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                      </select>
                    </div>
                    <div class="col-md-3">
                    <div class="btn-hol">
                      <button class="btn" type="submit">Search Now!</button>
                    </div>
                  </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="row services-holder">
          <div class="col-md-6 col-lg-4 mb-md-4">
            <div class="service-card text-left">
              <div class="img-hol overflow-hidden">
                <a href="#">
                  <img class="scaling w-100" src="images/service1.jpg" alt="Service Alarm">
                </a>
              </div>
              <div class="service-details">
                <a href="#" class="title d-block mb-4">Alarm Systems</a>
                <address>11-18 Panton St, London</address>
                <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
                <a href="#" class="btn">View Detail</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-md-4">
            <div class="service-card text-left">
              <div class="img-hol overflow-hidden">
                <a href="#">
                  <img class="scaling w-100" src="images/service1.jpg" alt="Service Alarm">
                </a>
              </div>
              <div class="service-details">
                <a href="#" class="title d-block mb-4">Alarm Systems</a>
                <address>11-18 Panton St, London</address>
                <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
                <a href="#" class="btn">View Detail</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-md-4">
            <div class="service-card text-left">
              <div class="img-hol overflow-hidden">
                <a href="#">
                  <img class="scaling w-100" src="images/service1.jpg" alt="Service Alarm">
                </a>
              </div>
              <div class="service-details">
                <a href="#" class="title d-block mb-4">Alarm Systems</a>
                <address>11-18 Panton St, London</address>
                <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
                <a href="#" class="btn">View Detail</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-md-4">
            <div class="service-card text-left">
              <div class="img-hol overflow-hidden">
                <a href="#">
                  <img class="scaling w-100" src="images/service1.jpg" alt="Service Alarm">
                </a>
              </div>
              <div class="service-details">
                <a href="#" class="title d-block mb-4">Alarm Systems</a>
                <address>11-18 Panton St, London</address>
                <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
                <a href="#" class="btn">View Detail</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-md-4">
            <div class="service-card text-left">
              <div class="img-hol overflow-hidden">
                <a href="#">
                  <img class="scaling w-100" src="images/service1.jpg" alt="Service Alarm">
                </a>
              </div>
              <div class="service-details">
                <a href="#" class="title d-block mb-4">Alarm Systems</a>
                <address>11-18 Panton St, London</address>
                <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
                <a href="#" class="btn">View Detail</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-md-4">
            <div class="service-card text-left">
              <div class="img-hol overflow-hidden">
                <a href="#">
                  <img class="scaling w-100" src="images/service1.jpg" alt="Service Alarm">
                </a>
              </div>
              <div class="service-details">
                <a href="#" class="title d-block mb-4">Alarm Systems</a>
                <address>11-18 Panton St, London</address>
                <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
                <a href="#" class="btn">View Detail</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-md-4">
            <div class="service-card text-left">
              <div class="img-hol overflow-hidden">
                <a href="#">
                  <img class="scaling w-100" src="images/service1.jpg" alt="Service Alarm">
                </a>
              </div>
              <div class="service-details">
                <a href="#" class="title d-block mb-4">Alarm Systems</a>
                <address>11-18 Panton St, London</address>
                <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
                <a href="#" class="btn">View Detail</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-md-4">
            <div class="service-card text-left">
              <div class="img-hol overflow-hidden">
                <a href="#">
                  <img class="scaling w-100" src="images/service1.jpg" alt="Service Alarm">
                </a>
              </div>
              <div class="service-details">
                <a href="#" class="title d-block mb-4">Alarm Systems</a>
                <address>11-18 Panton St, London</address>
                <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
                <a href="#" class="btn">View Detail</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-md-4">
            <div class="service-card text-left">
              <div class="img-hol overflow-hidden">
                <a href="#">
                  <img class="scaling w-100" src="images/service1.jpg" alt="Service Alarm">
                </a>
              </div>
              <div class="service-details">
                <a href="#" class="title d-block mb-4">Alarm Systems</a>
                <address>11-18 Panton St, London</address>
                <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
                <a href="#" class="btn">View Detail</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
@endsection