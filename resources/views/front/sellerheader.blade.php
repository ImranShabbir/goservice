<!-- Header -->

<header id="header">
    <div class="top-bar">
      <div class="container">
        <ul class="d-flex justify-content-center justify-content-lg-end align-iems-center">
          <li><a href="#"><span class="icon-heart"></span>My Wishlist</a></li>
          <li><a href="#"><span class="icon-cart"></span>Cart</a></li>
          <li><a href="#"><span class="icon-shop"></span>Shop a Service</a></li>
          <li><a href="{{url('/seller/register')}}"><span class="icon-bullhorn"></span>Become a Seller</a></li>
          <li><a href="#"><span class="icon-text"></span>Messages</a></li>
          <?php
          $getuser = '';
     		if(Auth::check()) {
            if(Auth::guard('web')->user()){
    			$getuser = Auth::guard('web')->user();
				$getuser = $getuser->firstname . ' '. $getuser->lastname;
      }
      if(Auth::guard('seller')->user()){
          $getuser = Auth::guard('seller')->user();
        $getuser = $getuser->firstname . ' '. $getuser->lastname;
      }
			?>



				<li class="nav-item dropdown">
                            <a id="navbarDropdown" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{$getuser}} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            	<a class="dropdown-item" href="{{ route('home') }}">
                                    {{ __('Dashboard') }}
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>

			<?php }else{?>

				<li><a href="{{url('/login')}}"><span class="icon-user"></span>Sign In</a></li>
			<?php }
          	?>

        </ul>
      </div>
    </div>
    
  </header>