@extends('layouts.app')

@section('content')
<main>
		<!-- Samart Home section -->

		<section class="smart-home">
			<div class="container">
				<div class="row">
					<div class="col-lg-5">
						<div class="slider-wrap mb-4">
							<div class="slider-for mb-4">
								<div class="item">
									<img src="{{asset('images/product1.png')}}" alt="image"  draggable="false"/>
								</div>
								<div class="item">
									<img src="{{asset('images/deal3.jpg')}}" alt="image" draggable="false"/>
								</div>
								<div class="item">
									<img src="{{asset('images/product1.png')}}" alt="image" draggable="false"/>
								</div>
								<div class="item">
									<img src="{{asset('images/deal3.jpg')}}" alt="image" draggable="false"/>
								</div>
								<div class="item">
									<img src="{{asset('images/product1.png')}}" alt="image" draggable="false"/>
								</div>
							</div>

							<div class="slider-nav">
								<div class="item-nav">
									<img src="{{asset('images/product1.png')}}" alt="image" draggable="false"/>
								</div>
								<div class="item-nav">
									<img src="{{asset('images/deal3.jpg')}}" alt="image" draggable="false"/>
								</div>
								<div class="item-nav">
									<img src="{{asset('images/product1.png')}}" alt="image" draggable="false"/>
								</div>
								<div class="item-nav">
									<img src="{{asset('images/deal3.jpg')}}" alt="image" draggable="false"/>
								</div>
								<div class="item-nav">
									<img src="{{asset('images/product1.png')}}" alt="image" draggable="false"/>
								</div>
							</div>
						</div>
						<div class="rating-block">
							
						</div>		
					</div>
					<div class="col-lg-7">
						<div class="smart-home-wrap">
							<h1>Vivint Smart Home</h1>
							<p>Select the security features you would like to include in your system. </p>
							<form action="#">
								<div class="smart-check d-md-flex align-items-center flex-wrap">
									<div class="form-group">
										<label class="smart-label">Securing doors
										  <input type="checkbox">
										  <span class="checkmark"></span>
										</label>
									</div>
									<div class="form-group">
										<label class="smart-label">Securing windows
										  <input type="checkbox">
										  <span class="checkmark"></span>
										</label>
									</div>
									<div class="form-group">
										<label class="smart-label">Motion detectors
										  <input type="checkbox">
										  <span class="checkmark"></span>
										</label>
									</div>
									<div class="form-group">
										<label class="smart-label">Fire/smoke protection
										  <input type="checkbox">
										  <span class="checkmark"></span>
										</label>
									</div>
									<div class="form-group">
										<label class="smart-label">Video surveillance - not recorded
										  <input type="checkbox">
										  <span class="checkmark"></span>
										</label>
									</div>
									<div class="form-group">
										<label class="smart-label">Video surveillance - recorded
										  <input type="checkbox">
										  <span class="checkmark"></span>
										</label>
									</div>
									<div class="form-group">
										<label class="smart-label">Video surveillance - remotely monitored
										  <input type="checkbox">
										  <span class="checkmark"></span>
										</label>
									</div>
									<div class="form-group">
										<label class="smart-label">Central Monitoring
										  <input type="checkbox">
										  <span class="checkmark"></span>
										</label>
									</div>
									<div class="form-group">
										<label class="smart-label">Wireless System
										  <input type="checkbox">
										  <span class="checkmark"></span>
										</label>
									</div>
									<div class="form-group">
										<label class="smart-label">Other
										  <input type="checkbox">
										  <span class="checkmark"></span>
										</label>
									</div>
								</div>
								<div class="form-row mb-4">
				                  <div class="col-md-4 mb-2">
				                    <select class="custom-select">
				                      <option selected="">How many doors</option>
				                      <option value="1">type</option>
				                      <option value="2">type</option>
				                      <option value="3">type</option>
				                    </select>
				                  </div>
				                  <div class="col-md-4 mb-2">
				                    <select class="custom-select">
				                      <option selected="">How many windows</option>
				                      <option value="1">One</option>
				                      <option value="2">Two</option>
				                      <option value="3">Three</option>
				                    </select>
				                  </div>
				                  <div class="col-md-4 mb-2">
				                  	<input type="date" id="start" name="trip-start" value="Time" class="time">
				                  </div>
				                </div>
				                <div class="cart-wrap d-md-flex align-items-center flex-wrap justify-content-center justify-content-md-start">
				                	<div class="price d-flex justify-content-between">
				                		<del class="dull-grey">&#36;80</del> <span class="orange">&#36;65/mo</span>
				                	</div>
				                	<div class="btn-hol text-center">
				                		<a href="#" class="btn text-uppercase"><span class="icon-cart"></span> Add to Cart</a>
				                	</div>
				                </div>
							</form>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="service-tabs">
							<ul class="nav nav-tabs d-block d-md-flex" id="myTab" role="tablist">
							  <li class="nav-item">
							    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Service Highlight</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">About Vivint Smart Home</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">(56) Reviews</a>
							  </li>
							</ul>
							<div class="tab-content" id="myTabContent">
							  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">Avenir Light is a clean and stylish font favored by designers. It's easy on the eyes and a great go to font for titles, paragraphs & more.</div>
							  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
							  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
							</div>
						</div>		
					</div>
				</div>
			</div>
		</section>

		<!-- Product Comparison -->

		<section class="services-block categories overflow-hidden text-center product-comparison">
      <div class="container">
      	<div class="row">
					<div class="col-12">
						<div class="heading text-center">
							<h1>Product Comparisons</h1>
						</div>
					</div>
				</div>
        <div class="row services-holder">
	        <div class="col-md-6 col-lg-4 mb-md-4" style="" aria-hidden="true" tabindex="0">
	          <div class="service-card text-left">
	            <div class="img-hol overflow-hidden">
	              <a href="#" tabindex="-1">
	                <img class="scaling w-100" src="{{asset('images/service1.jpg')}}" alt="Service Alarm">
	              </a>
	            </div>
	            <div class="service-details">
	              <a href="#" class="title d-block mb-4" tabindex="-1">Alarm Systems</a>
	              <address>11-18 Panton St, London</address>
	              <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
	              <a href="#" class="btn" tabindex="-1">View Detail</a>
	            </div>
	          </div>
	        </div>
	        <div class="col-md-6 col-lg-4 mb-md-4" style="" aria-hidden="true" tabindex="-1">
	          <div class="service-card text-left">
	            <div class="img-hol overflow-hidden">
	              <a href="#" tabindex="-1">
	                <img class="scaling w-100" src="{{asset('images/service1.jpg')}}" alt="Service Alarm">
	              </a>
	            </div>
	            <div class="service-details">
	              <a href="#" class="title d-block mb-4" tabindex="-1">Alarm Systems</a>
	              <address>11-18 Panton St, London</address>
	              <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
	              <a href="#" class="btn" tabindex="-1">View Detail</a>
	            </div>
	          </div>
	        </div>
	        <div class="col-md-6 col-lg-4 mb-md-4" style="" aria-hidden="true" tabindex="-1">
	          <div class="service-card text-left">
	            <div class="img-hol overflow-hidden">
	              <a href="#" tabindex="-1">
	                <img class="scaling w-100" src="{{asset('images/service1.jpg')}}" alt="Service Alarm">
	              </a>
	            </div>
	            <div class="service-details">
	              <a href="#" class="title d-block mb-4" tabindex="-1">Alarm Systems</a>
	              <address>11-18 Panton St, London</address>
	              <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
	              <a href="#" class="btn" tabindex="-1">View Detail</a>
	            </div>
	          </div>
	        </div>
        </div>
      </div>
    </section>


		<!-- Complementary Service section -->
		<section class="complementary-service">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="heading text-center">
							<h1>Complementary Services</h1>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="complementary-wrap text-center mb-4 mb-md-0">
							<div class="img-block mb-4">
								<img src="{{asset('images/complementary-icon1.png')}}" alt="image" class="m-auto">
							</div>
							<strong class="title d-block">See solar deals</strong>
						</div>
					</div>
					<div class="col-md-4">
						<div class="complementary-wrap text-center mb-4 mb-md-0">
							<div class="img-block  mb-4">
									<img src="{{asset('images/complementary-icon2.png')}}" alt="image" class="m-auto">
							</div>
							<strong class="title d-block">See pest control deals</strong>
						</div>
					</div>
					<div class="col-md-4">
						<div class="complementary-wrap text-center mb-4 mb-md-0">
							<div class="img-block  mb-4">
									<img src="{{asset('images/complementary-icon2.png')}}" alt="image" class="m-auto">
							</div>
							<strong class="title d-block">See window deals</strong>
						</div>	
					</div>
				</div>
			</div>
		</section>
  </main>

  @endsection