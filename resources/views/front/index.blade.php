@extends('layouts.app')

@section('content')


    <main>
    <section class="visual-block overflow-hidden text-center position-relative" style="background-image: url(images/banner.jpg);">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="content-hol">
              <h1>Find best pros for all your <br>home projects.</h1>
              <form action="#" class="d-flex flex-column flex-md-row justify-content-center align-items-center">
                <div class="form-group w-100 mb-3 mb-md-0 position-relative location">
                  <input type="text" class="form-control m-0 w-100 pr-5" placeholder="Where to Look">
                </div>
                <div class="form-group w-100 mb-3 mb-md-0"> 
                  <input type="text" class="form-control m-0 w-100" placeholder="What service do you need?">
                </div>
                <button type="submit" class="btn mb-0">Get Started!</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      <span class="scroll-btn">
        <a id="scroll-down" href="#scroll-down" class="mouse"><span></span></a>
      </span>
    </section>

    <!-- Best Deals -->
    <section class="best-deal-block overflow-hidden text-center">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="heading mb-4">
              <h1>Best Deals In Your Area</h1>
              <p>Find the best service pros in your area at the best price. With the lowest prices in the industry, you are bound to love our service.</p>
            </div>
          </div>
        </div>
        <div class="row bestseller-items mb-5">
          <div class="col-12">
            <div class="card border-0 position-relative">
              <a href="{{url('/category/detail')}}">
                <div class="icon-hol icon-water"></div>
                <div class="image-hol position-relative">
                  <div class="flag position-absolute">45%</div>
                  <img class="w-100" src="images/deal1.jpg" alt="deal in area">
                  <div class="caption">Plumbing</div>
                </div>
              </a>
            </div>
          </div>
          <div class="col-12">
            <div class="card border-0 position-relative">
              <a href="{{url('/category/detail')}}">
                <div class="icon-hol icon-spray"></div>
                <div class="image-hol position-relative">
                  <div class="flag position-absolute">45%</div>
                  <img class="w-100" src="images/deal2.jpg" alt="deal in area">
                  <div class="caption">Pest Control</div>
                </div>
              </a>
            </div>
          </div>
          <div class="col-12">
            <div class="card border-0 position-relative">
              <a href="{{url('/category/detail')}}">
                <div class="icon-hol icon-power"></div>
                <div class="image-hol position-relative">
                  <div class="flag position-absolute">45%</div>
                  <img class="w-100" src="images/deal3.jpg" alt="deal in area">
                  <div class="caption">HVAC</div>
                </div>
              </a>
            </div>
          </div>
          <div class="col-12">
            <div class="card border-0 position-relative">
              <a href="{{url('/category/detail')}}">
                <div class="icon-hol icon-power"></div>
                <div class="image-hol position-relative">
                  <div class="flag position-absolute">45%</div>
                  <img class="w-100" src="images/deal3.jpg" alt="deal in area">
                  <div class="caption">HVAC</div>
                </div>
              </a>
            </div>
          </div>
        </div>
        <div class="btnhol">
          <a href="#" class="btn">Get Started!</a>
        </div>
      </div>
    </section>

    <!-- Most Popular Services -->
    <section class="services-block overflow-hidden text-center">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="heading mb-5">
              <h1>Most Popular Services</h1>
              <p>Find the best service pros in your area at the best price. With the lowest prices in the industry, you are bound to love our service.</p>
            </div>
          </div>
        </div>
        <div class="row services-holder">
          <div class="col-md-6 col-lg-4 mb-md-4">
            <div class="service-card text-left">
              <div class="img-hol overflow-hidden">
                <a href="#">
                  <img class="scaling w-100" src="images/service1.jpg" alt="Service Alarm">
                </a>
              </div>
              <div class="service-details">
                <a href="{{url('/category/detail')}}" class="title d-block mb-4">Alarm Systems</a>
                <address>11-18 Panton St, London</address>
                <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
                <a href="{{url('/category/detail')}}" class="btn">View Detail</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-md-4">
            <div class="service-card text-left">
              <div class="img-hol overflow-hidden">
                <a href="#">
                  <img class="scaling w-100" src="images/service1.jpg" alt="Service Alarm">
                </a>
              </div>
              <div class="service-details">
                <a href="{{url('/category/detail')}}" class="title d-block mb-4">Alarm Systems</a>
                <address>11-18 Panton St, London</address>
                <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
                <a href="{{url('/category/detail')}}" class="btn">View Detail</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-md-4">
            <div class="service-card text-left">
              <div class="img-hol overflow-hidden">
                <a href="#">
                  <img class="scaling w-100" src="images/service1.jpg" alt="Service Alarm">
                </a>
              </div>
              <div class="service-details">
                <a href="{{url('/category/detail')}}" class="title d-block mb-4">Alarm Systems</a>
                <address>11-18 Panton St, London</address>
                <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
                <a href="{{url('/category/detail')}}" class="btn">View Detail</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-md-4">
            <div class="service-card text-left">
              <div class="img-hol overflow-hidden">
                <a href="#">
                  <img class="scaling w-100" src="images/service1.jpg" alt="Service Alarm">
                </a>
              </div>
              <div class="service-details">
                <a href="{{url('/category/detail')}}" class="title d-block mb-4">Alarm Systems</a>
                <address>11-18 Panton St, London</address>
                <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
                <a href="{{url('/category/detail')}}" class="btn">View Detail</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-md-4">
            <div class="service-card text-left">
              <div class="img-hol overflow-hidden">
                <a href="#">
                  <img class="scaling w-100" src="images/service1.jpg" alt="Service Alarm">
                </a>
              </div>
              <div class="service-details">
                <a href="{{url('/category/detail')}}" class="title d-block mb-4">Alarm Systems</a>
                <address>11-18 Panton St, London</address>
                <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
                <a href="{{url('/category/detail')}}" class="btn">View Detail</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-md-4">
            <div class="service-card text-left">
              <div class="img-hol overflow-hidden">
                <a href="#">
                  <img class="scaling w-100" src="images/service1.jpg" alt="Service Alarm">
                </a>
              </div>
              <div class="service-details">
                <a href="{{url('/category/detail')}}" class="title d-block mb-4">Alarm Systems</a>
                <address>11-18 Panton St, London</address>
                <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
                <a href="{{url('/category/detail')}}" class="btn">View Detail</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-md-4">
            <div class="service-card text-left">
              <div class="img-hol overflow-hidden">
                <a href="#">
                  <img class="scaling w-100" src="images/service1.jpg" alt="Service Alarm">
                </a>
              </div>
              <div class="service-details">
                <a href="{{url('/category/detail')}}" class="title d-block mb-4">Alarm Systems</a>
                <address>11-18 Panton St, London</address>
                <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
                <a href="{{url('/category/detail')}}" class="btn">View Detail</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-md-4">
            <div class="service-card text-left">
              <div class="img-hol overflow-hidden">
                <a href="#">
                  <img class="scaling w-100" src="images/service1.jpg" alt="Service Alarm">
                </a>
              </div>
              <div class="service-details">
                <a href="{{url('/category/detail')}}" class="title d-block mb-4">Alarm Systems</a>
                <address>11-18 Panton St, London</address>
                <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
                <a href="{{url('/category/detail')}}" class="btn">View Detail</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-md-4">
            <div class="service-card text-left">
              <div class="img-hol overflow-hidden">
                <a href="#">
                  <img class="scaling w-100" src="images/service1.jpg" alt="Service Alarm">
                </a>
              </div>
              <div class="service-details">
                <a href="{{url('/category/detail')}}" class="title d-block mb-4">Alarm Systems</a>
                <address>11-18 Panton St, London</address>
                <span class="price d-block"><del class="mr-2">$44.96</del> From <strong class="orange ml-2">$19.99</strong></span>
                <a href="{{url('/category/detail')}}" class="btn">View Detail</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Home Care Block -->
    <section class="homecare-block overflow-hidden text-center">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="heading mb-lg-5">
              <h1>The Best Way to Care for <br class="d-none d-lg-block">Your Home</h1>
            </div>
          </div>
        </div>
        <div class="row align-items-lg-center">
          <div class="col-lg-6">
            <div class="img-hol mb-5 mb-lg-0 position-relative">
              <img class="w-100" src="images/home.jpg" alt="home service">
            </div>
          </div>
          <div class="col-lg-6">
            <div class="content-hol">
              <div class="content position-relative text-lg-left mb-5">
                <div class="icon-hol icon-label m-auto"></div>
                <span>Guaranteed lowest prices, upfront</span>
                <p>Avenir Light is a clean and stylish font favored by designers. It's easy on the eyes and a great go to font for titles, paragraphs &amp; more.</p>
              </div>
              <div class="content position-relative text-lg-left mb-5">
                <div class="icon-hol icon-calender m-auto"></div>
                <span>Schedule your pro at your convenience</span>
                <p>Avenir Light is a clean and stylish font favored by designers. It's easy on the eyes and a great go to font for titles, paragraphs &amp; more.</p>
              </div>
              <div class="content position-relative text-lg-left mb-5">
                <div class="icon-hol icon-lock m-auto"></div>
                <span>Secure Payment</span>
                <p>Avenir Light is a clean and stylish font favored by designers. It's easy on the eyes and a great go to font for titles, paragraphs &amp; more.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="industries-block overflow-hidden text-center position-relative">
        <div class="row">
          <div class="col-12 heading">
            <h1>Trusted by some of the top service providers<br class="d-none d-lg-block"> across many industries</h1>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="logo-contain bg-white">
                <div class="logo-wrap d-flex justify-content-between pb-3">
                  <div class="item">
                    <img src="images/indus01.png" class="m-auto" alt="img-description">
                  </div>
                  <div class="item">
                    <img src="images/indus02.png" class="m-auto" alt="img-description">
                  </div>
                  <div class="item">
                    <img src="images/indus03.png" class="m-auto" alt="img-description">
                  </div>
                  <div class="item">
                    <img src="images/indus04.png" class="m-auto" alt="img-description">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

    <!-- SignIn Block -->
    <section class="signin-block overflow-hidden">
      <div class="container">
        <div class="row flex-row-reverse align-items-center">
          <div class="col-md-6">
            <div class="img-hol">
              <img src="images/signup.jpg" alt="image">
            </div>
          </div>
          <div class="col-md-6">
            <div class="content-hol">
              <h1>Sign up to be a GO SERVICES</h1>
              <p>Avenir Light is a clean and stylish font favored by designers. It's easy on the eyes and a great go to font for titles, paragraphs & more.</p>
              <a href="{{url('/register')}}" class="btn">Learn More!</a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
@endsection
