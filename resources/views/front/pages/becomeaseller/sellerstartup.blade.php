@extends('layouts.sellerlanding')

@section('content')

		<main>
    <section class="visual-block inner-visual overflow-hidden text-center position-relative" style="background-image: url(../images/banner.jpg);">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="content-hol">
              <h1 class="mb-5">The Easiest Way to Connect With <br class="d-none d-md-block">New Customers</h1>
              <a href="{{url('/seller/register')}}" class="btn">Become a Seller!</a>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- How it Works Block -->
    <section class="howitworks overflow-hidden text-center">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="head mb-5">
              <h1>How it <span class="orange">works?</span></h1>
            </div>
          </div>
        </div>
        <div class="row mb-md-5">
          <div class="col-md-4 content mb-5 mb-md-0">
            <div class="icon-hol icon-correct"></div>
            <span>Verify Company Information</span>
            <p>Avenir Light is a clean and stylish font favored by designers. It's easy on the eyes and a great go to font for titles, paragraphs & more.</p>
          </div>
          <div class="col-md-4 content mb-5 mb-md-0">
            <div class="icon-hol icon-settings"></div>
            <span>Register Your Service</span>
            <p>Avenir Light is a clean and stylish font favored by designers. It's easy on the eyes and a great go to font for titles, paragraphs & more.</p>
          </div>
          <div class="col-md-4 content">
            <div class="icon-hol icon-admin"></div>
            <span>Service New Accounts</span>
            <p>Avenir Light is a clean and stylish font favored by designers. It's easy on the eyes and a great go to font for titles, paragraphs & more.</p>
          </div>
        </div>
        <div class="btn-hol">
          <a href="{{url('/seller/register')}}" class="btn bg-blue">Get Started!</a>
        </div>
      </div>
    </section>

    <!-- FAQs Block -->
    <section class="faq-block overflow-hidden text-center">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="head mb-5">
              <h1>Frequently Asked <span class="orange">Questions</span></h1>
            </div>
            <div class="accordion text-left" id="accordionExample">
              <div class="card rounded-0 inner-card">
                <div class="card-header p-0" id="headingOne">
                  <h2 class="mb-0">
                    <button class="btn btn-link w-100 text-left rounded-0" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      How do I add new question?
                    </button>
                  </h2>
                </div>
                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                  <div class="card-body">
                    To add a new question go to app settings and press "Manage Questions" button.
                  </div>
                </div>
              </div>
              <div class="card rounded-0 inner-card">
                <div class="card-header p-0" id="headingTwo">
                  <h2 class="mb-0">
                    <button class="btn btn-link w-100 text-left rounded-0 collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      Can I insert pictures in my FAQ?
                    </button>
                  </h2>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                  <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                  </div>
                </div>
              </div>
              <div class="card rounded-0 inner-card">
                <div class="card-header p-0" id="headingThree">
                  <h2 class="mb-0">
                    <button class="btn btn-link w-100 text-left rounded-0 collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      Can I insert videos in my FAQ?
                    </button>
                  </h2>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                  <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>


@endsection