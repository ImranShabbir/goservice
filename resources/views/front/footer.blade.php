<footer id="footer" class="overflow-hidden position-relative">
  <div class="footerlinksoutter position-relative pb-lg-4">
    <div class="container">       
      <div class="row footer-links-box text-white text-center text-md-left">
        <div class="col-md-6 col-lg-3">
          <div class="link-hol">
            <span class="title d-block text-capitalize">contact us</span>
            <ul class="ulwithicons">
              <li><a href="tel:03339875259"><span class="icons icon-phone"></span>0333 9875259</a></li>
              <li><a href="mailto:amityyang1996@gmail.com"><span class="icons icon-mail"></span>support@meeniful.com</a></li>
            </ul>
            <div class="logo m-auto m-md-0 mt-5 mt-md-5">
              <a href="#">
                <img class="m-auto m-md-0" src="{{asset('images/logo.png')}}" alt="logo">
              </a>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-3">
          <div class="link-hol">
            <span class="title d-block text-capitalize">Category</span>
            <ul>
               
              <li><a href="{{url('/category/detail')}}">Alarms</a></li>
                
              <li><a href="{{url('/category/detail')}}">Carpets</a></li>
                
              <li><a href="{{url('/category/detail')}}">HVAC</a></li>
                
              <li><a href="{{url('/category/detail')}}">Painting</a></li>
                
              <li><a href="{{url('/category/detail')}}">Pest Control</a></li>
                
              <li><a href="{{url('/category/detail')}}">Plumbing</a></li>
                
              <li><a href="{{url('/category/detail')}}">Solar</a></li>

              <li><a href="{{url('/category/detail')}}">Windows</a></li>

              <li><a href="#">See All Categories</a></li>
              
            </ul>
          </div>
        </div>
        <div class="col-md-6 col-lg-2">
          <div class="link-hol">
            <span class="title d-block text-capitalize">Links</span>
            <ul>
                
              <li><a href="{{url('/')}}">home</a></li>
                
              <li><a href="{{url('/about')}}">about</a></li>
                
              <li><a href="#">contact</a></li>
                
              <li><a href="#">blog</a></li>

              <li><a href="#">Terms &amp; Conditions</a></li>
                
              <li><a href="#">Privacy Policy</a></li>
              
              
            </ul>
          </div>
        </div>
        <div class="col-md-6 col-lg-4">
          <div class="link-hol">
            <span class="title d-block text-capitalize">Get Social With Us</span>
          </div>
          <ul class="social d-flex align-items-center justify-content-center justify-content-md-start">
            <li><a class="icon-facebook" href="#"></a></li>
            <li><a class="icon-twitter" href="#"></a></li>
            <li><a class="icon-google-plus" href="#"></a></li>
            <li><a class="icon-instagram" href="#"></a></li>
            <li><a class="icon-pinterest" href="#"></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="copyright-hol">
    <div class="container">
      <div class="row text-center align-items-center justify-content-between">
        <div class="col-12">
          <p class="copyright text-center m-0">© HPM Inc. All Right Reserved.   |   Website Designed by
            <a class="made-by" href="https://www.htmlpro.net/" rel="nofollow" target="_blank">HTML Pro</a></p>
        </div> 
      </div>
    </div>
  </div>
</footer>
