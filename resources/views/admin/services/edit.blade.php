@extends('layouts.admindashboard')
@section('content')

           <!-- Hero -->
                <div class="bg-body-light">
                    <div class="content content-full">
                        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                            <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Edit Service</h1>
                            <!-- <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">Tables</li>
                                    <li class="breadcrumb-item active" aria-current="page">DataTables</li>
                                </ol>
                            </nav> -->
                        </div>
                    </div>
                </div>
                <!-- END Hero -->

                <!-- Page Content -->
                <div class="content">
                    <!-- jQuery Validation (.js-validation class is initialized in js/pages/be_forms_validation.min.js which was auto compiled from _es6/pages/be_forms_validation.js) -->
                    <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
                    <form class="js-validation" id="updateService">
                        <input type="hidden" name="id" id="service_id" value="{{$service->id}}">
                        <div class="block block-rounded block-bordered">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Edit Service</h3>
                                
                            </div>
                            <div class="block-content block-content-full">
                                <div class="">
                                    <!-- Regular -->
                                    <h2 class="content-heading">Fill all fields with *</h2>
                                    <div class="row items-push">
                                       
                                        <div class="col-lg-8 col-xl-5">
                                            <div class="form-group">
                                                <label for="val-username">Service Name <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="service_name" name="service_name" placeholder="Enter a Service name" value="{{$service->service_name}}">

                                            </div>

                                            <div class="alert alert-danger print-error-msg" style="display:none">
                                                 <ul></ul>
                                             </div>
                                           
                                            <!-- Submit -->
                                    
                                            <button type="button" id="updateBtnService" class="btn btn-primary">Update</button>
                                        
                                   
                                    <!-- END Submit -->
                                        </div>
                                    </div>
                                    <!-- END Regular -->

                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- jQuery Validation -->
                </div>
                <!-- END Page Content -->
@endsection
