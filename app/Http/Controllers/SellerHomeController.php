<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Auth\middleware\EnsureSellerEmailIsVerified;
use App\Seller;
use Auth;

class SellerHomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        //$this->middleware('auth:seller');

        $this->middleware(['auth:seller']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       
        $user = Auth::user('seller')->toArray();

        if ( $user['registration_steps'] == 0 ) {// do your magic here
            return redirect()->route('seller.business.registration');
        }

        return view('seller_dashboard');
    }

    public function completeRegistration()
    {
        return view('registration_steps');
    }
}
