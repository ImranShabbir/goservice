<?php


namespace App\Http\Controllers\Auth\Admin;
use App\Seller;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;

class AdminVerificationController extends Controller
{
    // use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    // public function show(Request $request)
    // {

       
    //     return $request->user('seller')->hasVerifiedEmail()
    //                     ? redirect($this->redirectPath())
    //                     : view('auth.seller.verify');
    // }

    public function show(Request $request)
    {
        return $request->user('admin')->hasVerifiedEmail()
            ? redirect()->route('admin.dashboard')
            : view('auth.admin.verify',[
                'resendRoute' => 'admin.verification.resend',
            ]);
    }


    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    // public function verify(Request $request)
    // {
    //     if ($request->route('id') != $request->user('seller')->getKey()) {
    //         throw new AuthorizationException;
    //     }

    //     if ($request->user()->hasVerifiedEmail()) {
    //         return redirect($this->redirectPath());
    //     }

    //     if ($request->user()->markEmailAsVerified()) {
    //         event(new Verified($request->user()));
    //     }

    //     return redirect($this->redirectPath())->with('verified', true);
    // }

    public function verify(Request $request)
    {
        if ($request->route('id') != $request->user('admin')->getKey()) {
            //id value doesn't match.
            return redirect()
                ->route('admin.verification.notice')
                ->with('error','Invalid user!');
        }

        if ($request->user('admin')->hasVerifiedEmail()) {
            return redirect()
                ->route('admin.dashboard');
        }

        $request->user('admin')->markEmailAsVerified();

        return redirect()
            ->route('admin.dashboard')
            ->with('status','Thank you for verifying your email!');
    }

    /**
     * Resend the email verification notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function resend(Request $request)
    // {
    //     if ($request->user()->hasVerifiedEmail()) {
    //         return redirect($this->redirectPath());
    //     }

    //     $request->user()->sendEmailVerificationNotification();

    //     return back()->with('resent', true);
    // }

    public function resend(Request $request)
    {
        if ($request->user('admin')->hasVerifiedEmail()) {
            return redirect()->route('admin.dashboard');
        }

        $request->user('admin')->sendEmailVerificationNotification();

        return redirect()
            ->back()
            ->with('status','We have sent you a verification email!');
    }
}
