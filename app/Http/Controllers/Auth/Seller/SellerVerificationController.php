<?php


namespace App\Http\Controllers\Auth\Seller;
use App\Seller;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;

class SellerVerificationController extends Controller
{
    // use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/seller/business/registration';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:seller');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    public function show(Request $request)
    {
        return $request->user('seller')->hasVerifiedEmail()
            ? redirect()->route('seller.business.registration')
            : view('auth.seller.verify',[
                'resendRoute' => 'seller.verification.resend',
            ]);
    }



    public function verify(Request $request)
    {
        if ($request->route('id') != $request->user('seller')->getKey()) {
            //id value doesn't match.
            return redirect()
                ->route('seller.verification.notice')
                ->with('error','Invalid user!');
        }

        if ($request->user('seller')->hasVerifiedEmail()) {
            return redirect()
                ->route('seller.business.registration');
        }

        $request->user('seller')->markEmailAsVerified();

        return redirect()
            ->route('seller.business.registration')
            ->with('status','Thank you for verifying your email!');
    }


    public function resend(Request $request)
    {
        if ($request->user('seller')->hasVerifiedEmail()) {
            return redirect()->route('seller.dashboard');
        }

        $request->user('seller')->sendEmailVerificationNotification();

        return redirect()
            ->back()
            ->with('status','We have sent you a verification email!');
    }
}
