<?php

namespace App\Http\Controllers\Auth\Seller;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SellerLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/seller/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:seller')->except('logout');
    }

    public function showLoginForm()
    {

        return view('auth.seller.login');
    }

    protected function attemptLogin(Request $request)
    {

        echo "login login";

        //exit();
        return $this->guard('seller')->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    protected function guard()
    {
        return Auth::guard('seller');
    }

    public function logout(Request $request) {
        Auth::guard('seller')->logout();
         // $request->session()->invalidate();
        return redirect('seller/login');
    }


    protected function authenticated(Request $request, $user)
        {

            echo "check here";
            $data = $user->toArray();

            print_r($data['registration_steps']);

           // exit();
        if ( $data['registration_steps'] == 0 ) {// do your magic here
            return redirect()->route('seller.business.registration');
        }

         return redirect('/seller/dashboard');
        }

    
}

