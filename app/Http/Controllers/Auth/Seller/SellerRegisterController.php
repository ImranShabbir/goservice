<?php

namespace App\Http\Controllers\Auth\Seller;

use App\Seller;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Auth;
//use App\Http\Controllers\Redirect;

class SellerRegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/seller/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:seller');
    }

    public function showRegistrationForm()
    {

        return view('auth.seller.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:sellers'],
            'business_name' => ['required', 'string', 'max:255'],
            'business_description' => ['required', 'string', 'max:255'],
            'business_location' => ['required', 'string', 'max:255'],
            'business_website' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {


        $seller =  Seller::create([
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'business_name' => $data['business_name'],
            'business_description' => $data['business_description'],
            'business_location' => $data['business_location'],
            'business_website' => $data['business_website'],
            'business_logo' => 'test_logo.jpg',
            'password' => Hash::make($data['password']),
            'user_type' => $data['user_type'],
        ]);
        //$seller->sendEmailVerificationNotification();

        return $seller;

        
    }

    public function register(Request $request)
    {

       // echo "check this";
        //exit();
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        
        $this->guard('seller')->login($user);


        return $this->registered($request, $user)
        ?: redirect($this->redirectPath());
    }

    public function login(Request $request)
    {

        $this->validator($request);

    //check if the user has too many login attempts.
        if ($this->hasTooManyLoginAttempts($request)){
        //Fire the lockout event.
            $this->fireLockoutEvent($request);

        //redirect the user back after lockout.
            return $this->sendLockoutResponse($request);
        }

    //attempt login.
        if(Auth::guard('seller')->attempt($request->only('email','password'),$request->filled('remember'))){
        //Authenticated
            return redirect()
            ->intended(route('seller.dashboard'))
            ->with('status','You are Logged in as seller!');
        }

    //keep track of login attempts from the user.
        $this->incrementLoginAttempts($request);

    //Authentication failed
        return $this->loginFailed();
    }

    protected function guard()
    {
        return Auth::guard('seller');
    }
    

    
}
