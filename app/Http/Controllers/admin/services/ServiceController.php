<?php

namespace App\Http\Controllers\admin\services;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\admin\Services;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services =  Services::all(); 
        return view('admin/services/all',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $messages = [
            'service_name.required' => 'Please enter your service name.',
            
         ];
         $validator = Validator::make($request->all(), [
             'service_name' => 'required|unique:services,service_name',
             
         ],$messages);
         if($validator->fails()){
             
             return response()->json(['error'=>$validator->errors()->all()]);
         }else{
                        
            Services::Create(
                 [
                     'service_name' => $request->service_name
                     
             ]);
 
             return response()->json(['success' => 'Service Added Successfully.']);
         }
        
    }


    public function add()
    {
        
        return view('admin/services/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo "this is store function";
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo "this is show function";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        
        $service =  Services::find($request->id); 
       
        return view('admin/services/edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {


        // echo "<pre>";
        // print_r($request->all());

        // exit();

        $messages = [
            'service_name.required' => 'Please enter your service name.',
            
         ];
        $validator = Validator::make($request->all(), [
            'service_name' => 'required|unique:services,service_name,'.$request->service_id
                                   
        ],$messages);
        if($validator->fails()){
            
            return response()->json(['error'=>$validator->errors()->all()]);
        }else{
                        
            $service =  Services::find($request->service_id);

            $service->service_name = $request->service_name;
            
            $service->save();             
           
            return response()->json(['success' => 'Service Updated Successfully.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $service = Services::find($request->id);
       
        $service->delete();

        return response()->json(['success' => 'Service Deleted Successfully.']);
    }
}
