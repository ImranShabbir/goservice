<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    protected $table = 'services';

    protected $fillable = [
        'service_name'
    ];
    protected $hidden = ['created_at', 'updated_at'];

}
