<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\SellerResetPasswordNotification;
use App\Notifications\SellerEmailVerificationNotification;
// use Illuminate\Auth\middleware\EnsureSellerEmailIsVerified;


class Seller extends Authenticatable implements MustVerifyEmail
{
     use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new SellerResetPasswordNotification($token));
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new SellerEmailVerificationNotification);
    }


    protected $guard = 'seller';

    protected $fillable = [
        'firstname', 'lastname', 'email', 'business_name', 'business_description','business_location','business_website','business_logo','password','user_type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


}
